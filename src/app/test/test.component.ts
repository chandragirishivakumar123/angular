import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  person: any;

  constructor() {
    this.person = {
      id: 101,
      name: 'Harsha',
      avg: 45.45,
      address:{streetNo:101, city:'Hyd', state:'Telangana'},
      hobbies:['Movies', 'Music', 'Swimming'] 
    };
  }

  ngOnInit(){
  }

  buttonSubmit() {
    //alert("Button Clicked...");
    console.log(this.person);
  }

}











